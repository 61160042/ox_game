
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dell
 */
class Player implements Serializable {

    private char name;
    private int win;
    private int lose;
    private int draw;

    public Player(char name) {
        this.name = name;
        win = 0;
        lose = 0;
        draw = 0;
    }

    @Override
    public String toString() {
        return "win=" + win + ", lose=" + lose + ", draw=" + draw;
    }

    public char getName() {
        return name;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void win() {
        win++;
    }

    public void lose() {
        lose++;
    }

    public void draw() {
        draw++;
    }
}
